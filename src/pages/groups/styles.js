import { makeStyles } from '@material-ui/styles';

export default makeStyles((theme) => ({
  titleBold: {
    fontWeight: 600,
  },
  tab: {
    color: theme.palette.primary.light + 'CC',
  },
  roleContainer: {
    boxShadow: theme.customShadows.widget,
    overflow: 'hidden',
    paddingBottom: theme.spacing(2),
  },
  dashedBorder: {
    border: '1px dashed',
    borderColor: theme.palette.primary.main,
    padding: theme.spacing(2),
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    marginTop: theme.spacing(1),
  },
  text: {
    marginBottom: theme.spacing(2),
  },
  root: {
    padding: theme.spacing(3, 2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));
