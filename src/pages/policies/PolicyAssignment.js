import {
  EuiButton,
  EuiFlexGroup,
  EuiFlexItem,
  EuiForm,
  EuiFormRow,
  EuiSelectable,
  EuiSpacer,
} from '@elastic/eui';
import React, { Fragment, memo } from 'react';

const PolicyAssignment = memo(
  ({
    optPolicies,
    setOptPolicies,
    optStdFields,
    setOptStdFields,
    onPolicyAssignment,
  }) => {
    return (
      <>
        <EuiForm component="form">
          <EuiFlexGroup component="span">
            <EuiFlexItem component="span">
              <EuiFormRow label="Policies" fullWidth>
                <EuiSelectable
                  searchable
                  singleSelection={true}
                  options={optPolicies}
                  onChange={(newOptions) => setOptPolicies(newOptions)}
                >
                  {(list, search) => (
                    <Fragment>
                      {search}
                      {list}
                    </Fragment>
                  )}
                </EuiSelectable>
              </EuiFormRow>
            </EuiFlexItem>
            <EuiFlexItem component="span">
              <EuiFormRow label="Standard fields" fullWidth>
                <EuiSelectable
                  searchable
                  options={optStdFields}
                  onChange={(newOptions) => setOptStdFields(newOptions)}
                >
                  {(list, search) => (
                    <Fragment>
                      {search}
                      {list}
                    </Fragment>
                  )}
                </EuiSelectable>
              </EuiFormRow>
            </EuiFlexItem>
          </EuiFlexGroup>
          <EuiSpacer />
          <EuiButton type="submit" onClick={onPolicyAssignment} fill>
            Save
          </EuiButton>
        </EuiForm>
      </>
    );
  }
);

export default PolicyAssignment;
