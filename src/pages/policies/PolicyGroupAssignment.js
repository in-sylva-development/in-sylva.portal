import {
  EuiButton,
  EuiFlexGroup,
  EuiFlexItem,
  EuiForm,
  EuiFormRow,
  EuiSelectable,
  EuiSpacer,
} from '@elastic/eui';
import React, { Fragment, memo } from 'react';

const PolicyGroupAssignment = memo(
  ({ optPolicies, setOptPolicies, optGroups, setOPtGroups, onPolicyGroupAssignment }) => {
    return (
      <>
        <EuiForm component="form">
          <EuiFlexGroup component="span">
            <EuiFlexItem component="span">
              <EuiFormRow label="Policies" fullWidth>
                <EuiSelectable
                  searchable
                  singleSelection={true}
                  options={optPolicies}
                  onChange={(newOptions) => setOptPolicies(newOptions)}
                >
                  {(list, search) => (
                    <Fragment>
                      {search}
                      {list}
                    </Fragment>
                  )}
                </EuiSelectable>
              </EuiFormRow>
            </EuiFlexItem>
            <EuiFlexItem component="span">
              <EuiFormRow label="Groups" fullWidth>
                <EuiSelectable
                  searchable
                  options={optGroups}
                  onChange={(newOptions) => setOPtGroups(newOptions)}
                >
                  {(list, search) => (
                    <Fragment>
                      {search}
                      {list}
                    </Fragment>
                  )}
                </EuiSelectable>
              </EuiFormRow>
            </EuiFlexItem>
          </EuiFlexGroup>
          <EuiSpacer />
          <EuiButton type="submit" onClick={onPolicyGroupAssignment} fill>
            Save
          </EuiButton>
        </EuiForm>
      </>
    );
  }
);

export default PolicyGroupAssignment;
