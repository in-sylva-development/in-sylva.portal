import React from 'react';
import { Grid, Paper, Typography, Button } from '@material-ui/core';
import classnames from 'classnames';
import useStyles from './styles';
import logo from './logo.png';

export default function Error403() {
  const classes = useStyles();

  return (
    <Grid container className={classes.container}>
      <div className={classes.logotype}>
        <img className={classes.logotypeIcon} src={logo} alt="logo" />
        <Typography variant="h3" color="white" className={classes.logotypeText}>
          In-Sylva Project
        </Typography>
      </div>
      <Paper classes={{ root: classes.paperRoot }}>
        <Typography
          variant="h1"
          color="primary"
          className={classnames(classes.textRow, classes.errorCode)}
        >
          403
        </Typography>
        <Typography variant="h5" color="primary" className={classes.textRow}>
          Forbidden
        </Typography>
        <Typography variant="h5" color="primary" className={classes.textRow}>
          Oops. Looks like you don't have the right permissions to access this page
        </Typography>
        <Button
          href="#/home"
          variant="contained"
          color="primary"
          size="large"
          className={classes.backButton}
        >
          Back to Home
        </Button>
      </Paper>
    </Grid>
  );
}
