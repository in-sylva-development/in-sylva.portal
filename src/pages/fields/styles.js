import { makeStyles } from '@material-ui/styles';

export default makeStyles((theme) => ({
  titleBold: {
    fontWeight: 600,
  },
  tab: {
    color: theme.palette.primary.light + 'CC',
  },
  iconsContainer: {
    boxShadow: theme.customShadows.widget,
    overflow: 'hidden',
    paddingBottom: theme.spacing(2),
  },
  text: {
    marginBottom: theme.spacing(2),
  },
  root: {
    padding: theme.spacing(3, 2),
  },

  body: {
    backgroundColor: '#f2ede8',
    margin: 0,
    padding: 0,
    fontSize: '16px',
    fontFamily: 'sans-serif',
    display: 'flex',
    height: '100vh',
  },
  FileUploadForm: {
    backgroundColor: '#fff',
    padding: '2rem',
    display: 'grid',
    gridGap: '1.6rem',
  },
  ActionBar: {
    textAlign: 'right',
  },

  ActionBarButton: {
    padding: '.4rem',
  },

  MessageBox: {
    padding: '1.6rem',
    backgroundColor: '#faf4e9',
    borderTop: '3px solid #ffa600',
  },
  input: {
    display: 'none',
  },
  dashedBorder: {
    border: '1px dashed',
    borderColor: theme.palette.primary.main,
    padding: theme.spacing(2),
    paddingTop: theme.spacing(4),
    paddingBottom: theme.spacing(4),
    marginTop: theme.spacing(1),
  },

  appBar: {
    position: 'relative',
  },
  layout: {
    width: 'auto',
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
    padding: theme.spacing(2),
    [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
      marginTop: theme.spacing(6),
      marginBottom: theme.spacing(6),
      padding: theme.spacing(3),
    },
  },
  stepper: {
    padding: theme.spacing(3, 0, 5),
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
}));
