## IN-SYLVA PORTAL

## Available Scripts

In the project directory, you can run:

### `npm run start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run lint`

Runs `eslint ./src` to analyze and find problems in source code.

[ESLint](https://eslint.org/docs/latest/) package is configured in `package.json`, under `eslintConfig`.

### `npm run lint:fix`

Runs `eslint --fix ./src` to analyze and find problems in source code and fix them automatically if possible.

### `npm run format`

Format code according to [Prettier](https://prettier.io/docs/en/) config, found in `package.json`, under `prettier`.
